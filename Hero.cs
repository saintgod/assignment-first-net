﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace first_csharp_assignment
{

    public abstract class Hero
    {
        public string name { get; set; }
        public int level { get; set; }
        public PrimaryAttributes attributes = new();
        public int TotalPrimary { get; set; }
        public Inventory inventory = new();
        public int characterDamage { get; set; }
        public abstract bool CheckIfCanEquipArmorThenEquip(Armor armor);
        public abstract bool CheckIfCanEquipWeaponThenEquip(Weapon weapon);

        public Hero()
        {
            level = 1;

        }
    }
}
