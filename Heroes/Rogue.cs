﻿using first_csharp_assignment.Custom_Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace first_csharp_assignment
{
    public class Rogue : Hero
    {
       
        public void levelUp()
        {
            level += 1;
            attributes.strength += 1;
            attributes.dexterity = 4;
            attributes.intelligence = 1;
        }
        public double CalculateDamage()
        {
            var item = inventory.ItemsInventory.ElementAt(3).Value;
            if (item != null)
            {
                Weapon weapon = item as Weapon;
                double primary = CalculatePrimaryStat();
                return (float)(weapon.baseDamage * weapon.attackSpeed) * (1 + (primary / 100));
            }
            return 1;
        }
        public double CalculatePrimaryStat()
        {
            double total = 0;
            for (int index = 0; index < inventory.ItemsInventory.Count - 1; index++)
            {
                var item = inventory.ItemsInventory.ElementAt(index);
                if (item.Value != null)
                {
                    Armor armor = item.Value as Armor;
                    total += armor.attributes.dexterity;
                }

            }
            return total + attributes.dexterity;
        }
        public override bool CheckIfCanEquipArmorThenEquip(Armor armorItem)
        {
            try
            {
                if (armorItem.armorType == ArmorType.Mail || armorItem.armorType == ArmorType.Leather)
                {
                    inventory.AddArmorToInventory(armorItem);
                    return true;
                }
                else
                {
                    throw new InvalidArmorException();
                }
            }
            catch(InvalidArmorException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
           
            return false;
        }
        public override bool CheckIfCanEquipWeaponThenEquip(Weapon weapon)
        {
            try
            {
                if ((weapon.weaponType == weapons.Dagger || weapon.weaponType == weapons.Sword) && weapon.reqLevel <= level)
                {
                    inventory.AddWeaponToInventory(weapon);
                    return true;
                }
                else
                {
                    throw new InvalidWeaponException();
                }
            }
            catch(InvalidWeaponException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
           
            return false;
        }
        public Rogue()
        {
            
            attributes.strength = 2;
            attributes.dexterity = 6;
            attributes.intelligence = 1;
        }
    }
    
}
