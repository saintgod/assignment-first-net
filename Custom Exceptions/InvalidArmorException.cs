﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace first_csharp_assignment.Custom_Exceptions
{
    public class InvalidArmorException : Exception
    {
        public override string Message => "Invalid Armor!";
    }
}
