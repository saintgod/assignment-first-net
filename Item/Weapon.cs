﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace first_csharp_assignment
{
    public enum weapons
    {
        Axe,
        Bow,
        Hammer,
        Staff,
        Sword,
        Wand,
        Dagger
    }
    public class Weapon : Item
    {
        public float attackSpeed { get; set; }
        public float baseDamage { get; set; }
        public weapons weaponType { get; set; }
        public Weapon()
        {
            
        }
    }
}
