﻿using first_csharp_assignment.Custom_Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace first_csharp_assignment
{
    public class Warrior : Hero
    {
     
        public void levelUp()
        {
            level += 1;
            attributes.strength += 3;
            attributes.dexterity += 2;
            attributes.intelligence += 1;
        }
        public double CalculateDamage()
        {
            var item = inventory.ItemsInventory.ElementAt(3).Value;
            if (item != null)
            {
                Weapon weapon = item as Weapon;
                double primary = CalculatePrimaryStat();
                return (float)(weapon.baseDamage * weapon.attackSpeed) * (1 + (primary / 100));
            }
            return 1;
        }
        public double CalculatePrimaryStat()
        {
            double total = 0;
            for (int index = 0; index < inventory.ItemsInventory.Count-1; index++)
            {                
                var item = inventory.ItemsInventory.ElementAt(index);
                if (item.Value != null)
                {
                    Armor armor = item.Value as Armor;
                    total += armor.attributes.strength;
                }
               
            }
            return total + attributes.strength;
        }
        public override bool CheckIfCanEquipArmorThenEquip(Armor armorItem)
        {
            try
            {
                if ((armorItem.armorType == ArmorType.Mail || armorItem.armorType == ArmorType.Plate) && armorItem.reqLevel <= level)
                {
                    inventory.AddArmorToInventory(armorItem);
                    return true;
                }
                else
                {
                    throw new InvalidArmorException();
                }

                
            }
            catch(IOException)
            {
                Console.WriteLine("Invalid Weapon");
            }
            //catch(Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            return false;
        }
        public override bool CheckIfCanEquipWeaponThenEquip(Weapon weap)
        {
            try
            {
                if ((weap.weaponType == weapons.Axe || weap.weaponType == weapons.Hammer || weap.weaponType == weapons.Sword) && weap.reqLevel <= level)
                {
                    inventory.AddWeaponToInventory(weap);
                    return true;
                }
                else
                {
                    throw new InvalidWeaponException();
                }

            }
            catch (IOException)
            {
                Console.WriteLine("test");

            }
            //catch (IOException ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
           
            
            return false;
        }
        public Warrior()
        {
            attributes.strength = 5;
            attributes.dexterity = 2;
            attributes.intelligence = 1;
        }
    }
}
