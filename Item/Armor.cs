﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace first_csharp_assignment
{
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
   
    public class Armor: Item
    {
        public PrimaryAttributes attributes { get; set; }
        public ArmorType armorType { get; set; }

        public Armor()
        {
            //attributes = new PrimaryAttributes();
        }
    }
}
