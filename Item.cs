﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace first_csharp_assignment
{
    public enum EquipmentSlot
    {
        Head = 1,
        Body = 2,
        Legs = 3,
        Weapon = 4
    }
    public abstract class Item
    {
        public string name { get; set; }
        public int reqLevel { get; set; }
        public EquipmentSlot ItemSlot { get; set; }

        

        
    }
}
