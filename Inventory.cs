﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace first_csharp_assignment
{
    public class Inventory
    {
        public Dictionary<EquipmentSlot, Item> ItemsInventory;
        public void AddArmorToInventory(Armor armor)
        {
            foreach(KeyValuePair<EquipmentSlot, Item> item in ItemsInventory){ 
                if(item.Key == armor.ItemSlot)
                {
                    ItemsInventory[item.Key] = armor;
                    
                }
                    
            }
        }
        public void AddWeaponToInventory(Weapon weapon)
        {
            foreach (KeyValuePair<EquipmentSlot, Item> item in ItemsInventory)
            {
                if (item.Key == weapon.ItemSlot)
                {
                    ItemsInventory[item.Key] = weapon;

                }

            }
        }
        public Inventory()
        {
            InitializeInventory();
        }
        private void InitializeInventory()
        {
            ItemsInventory = new Dictionary<EquipmentSlot, Item>();
            
            ItemsInventory.Add(EquipmentSlot.Head, null);
            ItemsInventory.Add(EquipmentSlot.Body, null);
            ItemsInventory.Add(EquipmentSlot.Legs, null);
            ItemsInventory.Add(EquipmentSlot.Weapon, null);

        }
       
        public int GetInventorySize()
        {
            int count = 0;
            foreach(var item in ItemsInventory)
            {
                if (item.Value != null)
                    count++;
            }
            return count;
        }
        private void RemoveItem(EquipmentSlot slot)
        {
           
            ItemsInventory[slot] = null;
        }
    }
}
