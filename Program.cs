﻿using System;

namespace first_csharp_assignment
{
    class Program
    {
        static void Main(string[] args)
        {
            Warrior warrior = new Warrior();
            Console.WriteLine("Warrior level: " + warrior.level);
            Armor testPlateBody = new()
            {
                name = "Common plate body armor",
                reqLevel = 1,
                ItemSlot = EquipmentSlot.Body,
                armorType = ArmorType.Leather,
                attributes = new PrimaryAttributes() { strength = 1, dexterity = 1, intelligence = 1 },
            };
            Weapon testAxe = new Weapon()
            {
                name = "Common axe",
                reqLevel = 1,
                ItemSlot = EquipmentSlot.Weapon,
                weaponType = weapons.Axe,
                baseDamage= 7,
                attackSpeed = 1,
            };

            Console.WriteLine("Inventory Size: " + warrior.inventory.GetInventorySize());
            Console.WriteLine("Damage without weapon: " + warrior.CalculateDamage());
            warrior.CheckIfCanEquipArmorThenEquip(testPlateBody);
            warrior.CheckIfCanEquipWeaponThenEquip(testAxe);
            Console.WriteLine("Primary stat after: " + warrior.CalculatePrimaryStat());
            Console.WriteLine("Damage with weapon: " + warrior.CalculateDamage());
            Console.WriteLine("Inventory Size after: " + warrior.inventory.GetInventorySize());
            Console.WriteLine(testPlateBody.attributes.strength);
            Console.WriteLine(warrior.TotalPrimary);
            Console.ReadKey();
        }

    }
}
